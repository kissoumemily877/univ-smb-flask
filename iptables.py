# save this as iptables.py
from flask import Flask,render_template

app = Flask(__name__)

@app.route("/rules_filter")
def rules_filter():
    return render_template("rules_filter.html")

@app.route("/rules_nat")
def rules_nat():
    return render_template("rules_nat.html")

@app.route("/rules_nat_add")
def rules_nat_add():
    return "rules_nat_add.html"

@app.route("/alias")
def alias():
    return render_template("alias.html")

@app.route("/")
def home():
    return render_template("start.html")   

if __name__ == "__main__":
    app.run(debug=True)